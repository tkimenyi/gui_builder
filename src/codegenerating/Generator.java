package codegenerating;
import componenttree.ComponentItem;
import componenttree.ContainerItem;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.io.*;
import java.util.ArrayList;

import javax.swing.JComponent;


public class Generator {
	
	private String packageName = "package codegenerating;";
	private final String import1 = "import javax.swing.*;";
	private final String import2 = "import java.awt.*;";
	private final String className = "public class BuiltGui extends JPanel {";
	private final String closingBracket="}";
	private final String mainMethod = "public static void main(String[] args){"; 
	private final String mainFrameDeclaration = "JFrame frame = new JFrame(\"user gui\");";
	private ArrayList<String> generatedLines; 
	private StringBuilder codeToDeclare;
	private StringBuilder codeToAdd;
	private StringBuilder allCode;
	private StringBuilder codeOntoFrame;
	private BufferedWriter writer;
	
	public Generator(){
		 generatedLines= new ArrayList<String>();
		 codeToAdd= new StringBuilder();
		 codeToDeclare= new StringBuilder();
		 codeOntoFrame= new StringBuilder();
		 allCode = new StringBuilder();
	}
	
	//put the code in the file
	public void generateFile(String code){
		try{
			File codeFile = new File("src/codegenerating/BuiltGui.java");			
			if (!codeFile.exists()){
				codeFile.createNewFile();
			}
			FileWriter fw = new FileWriter(codeFile.getAbsoluteFile());
			writer = new BufferedWriter(fw);
			writer.append(code);
			writer.close();
		}catch (IOException e){
			e.printStackTrace();
		}
	}
	
	// array list of generated lines of code
	public void addCode(){
		generatedLines.add(packageName);
		generatedLines.add(import1);
		generatedLines.add(import2);
		generatedLines.add(className);
		generatedLines.add(mainMethod);
		generatedLines.add(mainFrameDeclaration);
		generatedLines.add(codeToDeclare.toString());
		generatedLines.add(codeToAdd.toString());
		generatedLines.add(codeOntoFrame.toString());
		generatedLines.add(closingBracket);
		generatedLines.add(closingBracket);
	}
	
	//get the code to put on the file
	public String getCode(){
		for(int i=0;i<generatedLines.size();i++){
			allCode.append((generatedLines.get(i))+"\n");
		}
		return allCode.toString();
	}
	
	//check if a variable is declared
	public boolean isDeclared(String componentName, String componentType){
		String toAdd = componentType + " "+ componentName + "=" + "new" + " " + componentType + "();";
		for(String s: generatedLines){
			if((s.equals(toAdd))){
				return true;
			}
		}
		return false;
	}
	
	// add declarations of variables
	public void addDeclarations(String componentName, String componentType ){
		String declare = componentType + " "+ componentName + "=" + "new" + " " + componentType + "();\n";
		if(!(isDeclared(componentName, componentType)))codeToDeclare.append(declare);
	}

	// generate code for setting the size of a particular component
	public void setSizeOfComponentCode(String comp, Dimension dim){
		int width = dim.width;
		int height = dim.height;
		if(height!=0 && width!=0){
			String setSizeCode = comp + ".setPreferredSize(" + "new Dimension(" + width + "," + height + "));\n"; 
			codeToAdd.append(setSizeCode);
		}
	}
	
	// method generator for handling event 
	public void methodGenerator(String methodName, String returnType){
		String methodSignature = "public " + returnType + " " + methodName + "{ \n\n\n\n" + "}";
		codeToAdd.append(methodSignature);
	}
	
	//method to create the layout
	public void setSelectedLayoutCode(ContainerItem item, LayoutManager layout){
		String name = item.getName().toLowerCase();
		String type = item.getType();
		addDeclarations(name,type);
		
		ArrayList<ComponentItem> children = ((ContainerItem) item).getChildren();
		
		if(layout instanceof BorderLayout){
			String codeToSetLayout = name+"."+"setLayout(new BorderLayout())";
			codeToAdd.append(codeToSetLayout);
			for(ComponentItem itemChild: children){
				setTreeGenerated(itemChild);
				String childName = itemChild.getName().toLowerCase();
				String position = itemChild.getBorderLocation();
				String codeToSetChildLayout = name +".add("+childName+",BorderLayout."+position.toUpperCase()+";\n";
				codeToAdd.append(codeToSetChildLayout);
			}
			
		}else if(layout instanceof GridLayout){
			int rows = ((GridLayout) layout).getRows();
			int cols = ((GridLayout) layout).getColumns();
			String codeToSetLayout = name+"."+"setLayout(new GridLayout( " + rows + ", " + cols +"));\n";
			codeToAdd.append(codeToSetLayout);
			for(ComponentItem itemChild: children){
				setTreeGenerated(itemChild);
				String childName = itemChild.getName().toLowerCase();
				int rowLoc = (int)itemChild.getGridLocation().getX();
				int colLoc = (int)itemChild.getGridLocation().getY();
				String codeToSetChildLayout = name +".add("+childName+"," + rowLoc + ", " + colLoc + ");\n";
				codeToAdd.append(codeToSetChildLayout);
			}
		}
		
	}
	
	//sets the properties from the tree structure
	public void setTreeGenerated(ComponentItem item){	
		
		JComponent jc = item.getComponent();
		Dimension dim = jc.getSize();
		setSizeOfComponentCode(item.getName().toLowerCase(), dim);
		
		if(item instanceof ContainerItem){
			LayoutManager layout = ((ContainerItem) item).getLayout();
			setSelectedLayoutCode((ContainerItem)item,layout);
		}else{
			String type = item.getType();
			String name = item.getName().toLowerCase();
			addDeclarations(name,type);
		}
	}
	
	// adds containers to the frame
	public void addToFrame(ComponentItem item){
		String name = item.getName().toLowerCase();
		String setFrameVisible = "frame.setVisible(true);\n";
		String setFrameSize = "frame.setSize(800,800);\n";
		String codeToFrame = "frame" + ".add"+"("+name+");\n";
		String exit = "frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);\n";
		codeOntoFrame.append(codeToFrame);
		codeOntoFrame.append(setFrameSize);
		codeOntoFrame.append(setFrameVisible);
		codeOntoFrame.append(exit);
	}
	
}
