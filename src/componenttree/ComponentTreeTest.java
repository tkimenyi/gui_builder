package componenttree;

import javax.swing.*;

import static org.junit.Assert.*;

import java.awt.*;

import org.junit.Test;

public class ComponentTreeTest {

	@Test
	public void test() {
		ComponentManager tree = new ComponentManager();
		assertEquals(tree.getSize(), 0);
		ContainerItem comp = new ContainerItem(new JPanel(), "JPanel", new Dimension(100,100));
		ControlItem button = new ControlItem(new JButton(),"JButton",new Dimension(100,100));
		ContainerItem mom = new ContainerItem(new JPanel(), "JFrame",new Dimension(100,100));
		tree.setRoot(mom);
		tree.addChild(mom, comp, "JPanel", new Dimension(600,600));
		tree.addChild(comp, button, "JButton", new Dimension (100,30));
		assertEquals(tree.getSize(), 2);
		ContainerItem root = tree.getRoot();
		assertEquals(root, mom);
		assertEquals(button.getParent(), comp);
		System.out.println(tree.toString());
	}
	
	private ComponentItem toTest(){
		ContainerItem root = new ContainerItem(new JPanel(), "JPanel",new Dimension(100,100));
		ContainerItem panel1 = new ContainerItem(new JPanel(), "JPanel",new Dimension(100,100));
		ContainerItem panel2 = new ContainerItem(new JPanel(), "JPanel",new Dimension(100,100));
		
		ControlItem area1 = new ControlItem(new JTextArea(), "JTextArea",new Dimension(100,100));

		ControlItem textfield1 = new ControlItem(new JTextField(), "JTextField",new Dimension(100,100));

		ControlItem button1 = new ControlItem(new JButton(), "JButton",new Dimension(100,100));
		panel1.addChildComponent(area1);
		panel1.addChildComponent(textfield1);
		panel1.addChildComponent(button1);
		root.addChildComponent(panel1);
		root.addChildComponent(panel2);
		
		ControlItem list1 = new ControlItem(new JList(), "JList",new Dimension(100,100));
		panel2.addChildComponent(list1);
		ControlItem spinner1 = new ControlItem(new JSpinner(), "JSpinner",new Dimension(100,100));
		panel2.addChildComponent(spinner1);
	
		return root;
	}

}
